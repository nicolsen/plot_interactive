# Interactive Plotter 

Lightweight, portable, interactive plotting for 1D data in CSV formatted input files

_Author_: Nic Olsen

## Requirements

Run `pip install -r requirements.txt`

## Demo Usage

Generate a sample `data.csv` and run the interactive plotter.

```
python create_input.py
python plot.py
```
