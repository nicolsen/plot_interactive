#!/usr/bin/env python3

"""
(c) 2021 Nic Olsen 
See LICENSE for full details
"""

from PyQt5.QtWidgets import *

from matplotlib import colors
from matplotlib.backends.backend_qt5agg import FigureCanvas, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

import numpy as np
from sys import argv
import pandas as pd

class PlotApp(QMainWindow):
	"""A class for interactively reading in an input file and showing plots
	"""
	def __init__(self):
		"""Setup main widget and add layouts
		"""
		super().__init__()

		# Basic setup
		self.main = QWidget()
		self.setCentralWidget(self.main)
		self.layout = QHBoxLayout(self.main)

		# Collection of lines in in the plot
		self.curves = {}

		# Window Title
		self.setWindowTitle("Plotter")

		self.layout.addLayout(self.setup_input_layout(), 1)
		self.layout.addLayout(self.setup_plot_layout(), 3)

		self.showMaximized()

	def setup_input_layout(self)->QVBoxLayout:
		"""Create and return a layout for controls
		Returns:
			QVBoxLayout of input controls
		"""

		input_layout = QVBoxLayout()

		# Title of plot
		self.title_input = QLineEdit()
		self.title_input_label = QLabel("&Title:")
		self.title_input_label.setBuddy(self.title_input)
		input_layout.addWidget(self.title_input_label)
		input_layout.addWidget(self.title_input)

		# Horizontal layout for file text box and browse button
		file_input_layout = QHBoxLayout()
		self.file_input = QLineEdit()
		self.file_input.setReadOnly(True)
		self.file_input.setStyleSheet("color: black;  background-color: rgba(200,200,200,255)")
		file_input_label = QLabel("&Input File:")
		file_input_label.setBuddy(self.file_input)
		self.file_input_browse = QPushButton("&Browse")
		self.file_input_browse.clicked.connect(self.load_file)
		self.file_input_browse.clicked.connect(QFileDialog)

		self.valid_file = False

		file_input_layout.addWidget(self.file_input)
		file_input_layout.addWidget(self.file_input_browse)
		
		input_layout.addWidget(file_input_label)
		input_layout.addLayout(file_input_layout)

		# X-axis selection from all series in the file
		self.xlabel_input = QComboBox()
		xlabel_input_label = QLabel("&xlabel:")
		xlabel_input_label.setBuddy(self.xlabel_input)

		input_layout.addWidget(xlabel_input_label)
		input_layout.addWidget(self.xlabel_input)

		# Y-axis selection from all series in the file
		self.ylabel_input = QComboBox()
		ylabel_input_label = QLabel("&ylabel:")
		ylabel_input_label.setBuddy(self.ylabel_input)

		input_layout.addWidget(ylabel_input_label)
		input_layout.addWidget(self.ylabel_input)

		# Color selection from all matplotlib colors
		self.color_input = QComboBox()
		self.color_input.addItems(colors.cnames.keys())
		color_input_label = QLabel("&Color:")
		color_input_label.setBuddy(self.color_input)

		input_layout.addWidget(color_input_label)
		input_layout.addWidget(self.color_input)

		# Buttons
		buttons_layout = QHBoxLayout()

		# Add current selection to plot button
		add_button = QPushButton("&Add To Plot")
		add_button.clicked.connect(self.add_to_plot)
		buttons_layout.addWidget(add_button)

		# Clear plot button
		clear_button = QPushButton("&Clear Plot")
		clear_button.clicked.connect(self.clear_plot)
		buttons_layout.addWidget(clear_button)

		input_layout.addLayout(buttons_layout)

		# List of current curves available to remove
		remove_layout = QHBoxLayout()
		
		remove_button = QPushButton("&Remove Curve")
		remove_button.clicked.connect(self.remove_curve)

		self.curve_select = QComboBox()
		self.curve_select.addItems(self.curves.keys())
		remove_layout.addWidget(self.curve_select)
		remove_layout.addWidget(remove_button)

		input_layout.addLayout(remove_layout)

		return input_layout

	def setup_plot_layout(self)->QVBoxLayout:
		"""Create and return a layout for the plot
		Returns:
			VBoxLayout with single plot canvas widget 
		"""
		plot_layout = QVBoxLayout()

		canvas = FigureCanvas(Figure())

		plot_layout.addWidget(NavigationToolbar(canvas, self)) # Standard plt toolbar
		plot_layout.addWidget(canvas)

		self.ax = canvas.figure.subplots()

		return plot_layout

	def load_file(self)->None:
		"""Open a system file browser to search for input file
		"""
		try:
			filename = QFileDialog.getOpenFileName()[0]
			if ".csv" not in filename.lower():
				raise Exception(f"Not a csv file: {filename}")

			self.file_input.setText(filename)
			with open(filename, 'r') as infile:
				fields = [t.strip() for t in infile.readline().split(',')]
			
			self.xlabel_input.clear()
			self.xlabel_input.addItems(fields)
			self.ylabel_input.clear()
			self.ylabel_input.addItems(fields)
			self.valid_file = True
		
		except:
			msg = QMessageBox()
			msg.setWindowTitle("Error")
			msg.setText(f"Unable to open file: {filename}\nPlease select a valid csv file.")
			msg.setIcon(QMessageBox.Critical)
			msg.exec_()
			self.valid_file = False

	def add_to_plot(self):
		if not self.valid_file:
			msg = QMessageBox()
			msg.setWindowTitle("Error")
			msg.setText("Please select a data file")
			msg.setIcon(QMessageBox.Critical)
			msg.exec_()
			return

		x_label, y_label = self.xlabel_input.currentText(), self.ylabel_input.currentText()
		df = pd.read_csv(self.file_input.text())[[x_label, y_label]] # TODO might be worth only reading select columns and checking for duplicate and order
		x, y = df.iloc[:,0], df.iloc[:,1]
		
		x_label = self.xlabel_input.currentText()
		y_label = self.ylabel_input.currentText()
		self.ax.set_title(self.title_input.text())
		self.ax.set_xlabel(x_label)
		self.ax.set_ylabel(y_label)
		curve_name = f"{x_label}_vs_{y_label}"
		if curve_name in self.curves:
			self.curves[curve_name].remove()

		self.curves[curve_name] = self.ax.plot(x, y, self.color_input.currentText(), label=curve_name)[0]
		self.curve_select.addItem(curve_name)
		self.ax.figure.canvas.draw()

	def remove_curve(self):
		remove_curve = self.curve_select.currentText()
		if remove_curve == '':
			return
			
		self.curves[remove_curve].remove()
		self.curves.pop(remove_curve)
		
		self.curve_select.clear()
		self.curve_select.addItems(self.curves.keys())
		self.ax.figure.canvas.draw()

	def clear_plot(self):
		self.curves = {}
		self.curve_select.clear()

		self.ax.clear()
		self.ax.figure.canvas.draw()

if __name__ == "__main__":
	qapp = QApplication(argv)
	app = PlotApp()
	qapp.exec_()
