"""
(c) 2021 Nic Olsen 
See LICENSE for full details
"""

import pandas as pd
import numpy as np

outfile = 'data.csv'

t = np.linspace(0, 10)
sin = np.sin(t)
cos = np.cos(t)

df = pd.DataFrame(np.transpose([t, sin, cos]), columns=['t','sin','cos'])
df.to_csv(outfile)
